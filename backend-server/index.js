const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const { Pool } = require('pg');
const cors = require('cors');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const app = express();
const server = http.createServer(app);
const io = socketIo(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST", "DELETE"]
  }
});

const pool = new Pool({
  user: 'myuser', // Replace with your PostgreSQL username
  host: '34.72.123.42', // Replace with your PostgreSQL host IP or domain
  database: 'myappdb', // Replace with your PostgreSQL database name
  password: 'paukatta', // Replace with your PostgreSQL password
  port: 5432,
});

app.use(cors({
  origin: "*",
  methods: ["GET", "POST", "DELETE"],
  credentials: true
}));
app.use(express.json());

// Log a message when the pool connects to the database
pool.on('connect', () => {
  console.log('Connected to PostgreSQL database');
});

// Login a user
app.post('/login', async (req, res) => {
  const { username, password } = req.body;
  try {
    const result = await pool.query('SELECT * FROM users WHERE username = $1', [username]);
    if (result.rows.length === 0) {
      return res.status(400).send('Invalid username or password');
    }
    const user = result.rows[0];
    const isValid = await bcrypt.compare(password, user.password);
    if (!isValid) {
      return res.status(400).send('Invalid username or password');
    }
    const token = jwt.sign({ id: user.id, username: user.username }, 'your_jwt_secret');
    res.json({ token });
  } catch (err) {
    console.error('Error logging in user:', err);
    res.status(500).send('Server error');
  }
});

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (!token) return res.sendStatus(401);

  jwt.verify(token, 'your_jwt_secret', (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
};

// Endpoint to fetch messages (protected)
app.get('/messages', authenticateToken, async (req, res) => {
  try {
    const result = await pool.query('SELECT * FROM messages ORDER BY timestamp ASC');
    res.json(result.rows);
  } catch (err) {
    console.error('Error fetching messages:', err);
    res.status(500).send('Server error');
  }
});

// Endpoint to delete a message (protected)
app.delete('/messages/:id', authenticateToken, async (req, res) => {
  const { id } = req.params;
  const { username } = req.user;

  try {
    const result = await pool.query('DELETE FROM messages WHERE id = $1 AND username = $2 RETURNING *', [id, username]);

    if (result.rowCount === 0) {
      return res.status(404).json({ message: 'Message not found or not authorized' });
    }

    res.json(result.rows[0]);
  } catch (err) {
    console.error('Error deleting message:', err);
    res.status(500).send('Server error');
  }
});

io.on('connection', (socket) => {
  console.log('New client connected');

  // Fetch initial messages from the database
  pool.query('SELECT * FROM messages ORDER BY timestamp ASC', (err, res) => {
    if (err) {
      console.error('Error fetching initial messages:', err);
    } else {
      console.log('Initial messages fetched successfully:', res.rows);
      socket.emit('initialMessages', res.rows);
    }
  });

  // Receive and store new message
  socket.on('sendMessage', (message) => {
    console.log('Received new message:', message);
    const { username, text } = message;
    const timestamp = new Date().toISOString();

    if (!username || !text) {
      console.error('Invalid message format:', message);
      return;
    }

    pool.query(
      'INSERT INTO messages (username, text, timestamp) VALUES ($1, $2, $3) RETURNING *',
      [username, text, timestamp],
      (err, res) => {
        if (err) {
          console.error('Error storing new message:', err);
        } else {
          console.log('New message stored successfully:', res.rows[0]);
          io.emit('receiveMessage', res.rows[0]);
        }
      }
    );
  });

  // Delete message
  socket.on('deleteMessage', (messageId) => {
    console.log('Received delete request for message:', messageId);
    pool.query('DELETE FROM messages WHERE id = $1 RETURNING *', [messageId], (err, res) => {
      if (err) {
        console.error('Error deleting message:', err);
      } else {
        console.log('Message deleted successfully:', res.rows[0]);
        io.emit('deleteMessage', messageId);
      }
    });
  });

  socket.on('disconnect', () => {
    console.log('Client disconnected');
  });
});

const PORT = 4000;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
