import React, { useState } from 'react';
import axios from 'axios';

const AuthPage = ({ setToken, setUsername }) => {
  const [usernameInput, setUsernameInput] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError('');
    try {
      const response = await axios.post(
        `http://34.31.128.150/login`,
        { username: usernameInput, password }
      );
      const { token } = response.data;
      localStorage.setItem('username', usernameInput);
      localStorage.setItem('token', token);
      setUsername(usernameInput);
      setToken(token);
    } catch (err) {
      setError(err.response ? err.response.data : 'Server error');
    }
  };

  const handleLostPassword = () => {
    // Implement logic to handle lost password scenario (e.g., redirect to password recovery page)
    // For simplicity, alerting here
    alert('Contact admin for password recovery.');
  };

  const handleContactAdmin = () => {
    // Implement logic to contact admin for registration (e.g., show contact form)
    // For simplicity, alerting here
    alert('Contact admin for registration.');
  };

  return (
    <div className="auth-page">
      <form onSubmit={handleSubmit}>
        <h2>Login to chatApp</h2>
        {error && <p className="error">{error}</p>}
        <input
          type="text"
          value={usernameInput}
          onChange={(e) => setUsernameInput(e.target.value)}
          placeholder="Username"
          required
        />
        <input
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Password"
          required
        />
        <button type="submit">Login</button>
        <p onClick={handleLostPassword}>Lost your password?</p>
        <p onClick={handleContactAdmin}>Register to Chat App</p>
      </form>
    </div>
  );
};

export default AuthPage;
