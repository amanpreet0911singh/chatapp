import React from 'react';
import './App.css'; // Import your CSS file

const LogoutButton = ({ handleLogout }) => {
  const handleClick = () => {
    handleLogout(); // Call parent component's handleLogout function
  };

  return (
    <button className="logout-button" onClick={handleClick}>Logout</button>
  );
};

export default LogoutButton;
