import React, { useState, useEffect } from 'react';
import ChatWindow from './ChatWindow';
import MessageInput from './MessageInput';
import AuthPage from './AuthPage';
import LogoutButton from './LogoutButton';
import { io } from 'socket.io-client';
import axios from 'axios';
import './App.css';

const socket = io('http://34.31.128.150:80');

const App = () => {
  const [messages, setMessages] = useState([]);
  const [username, setUsername] = useState(localStorage.getItem('username') || '');
  const [token, setToken] = useState(localStorage.getItem('token'));

  useEffect(() => {
    if (token) {
      axios.get('http://34.31.128.150:80/messages', {
        headers: { Authorization: `Bearer ${token}` }
      })
        .then(response => {
          setMessages(response.data);
        })
        .catch(error => {
          console.error('Error fetching initial messages:', error);
        });

      socket.on('initialMessages', (initialMessages) => {
        setMessages(initialMessages);
      });

      socket.on('receiveMessage', (message) => {
        setMessages((prevMessages) => [...prevMessages, message]);
      });

      socket.on('deleteMessage', (messageId) => {
        setMessages((prevMessages) => prevMessages.filter(message => message.id !== messageId));
      });

      return () => {
        socket.off('initialMessages');
        socket.off('receiveMessage');
        socket.off('deleteMessage');
      };
    }
  }, [token]);

  const handleSendMessage = (newMessage) => {
    const message = { username, text: newMessage };
    socket.emit('sendMessage', message);
  };

  const handleDeleteMessage = (messageId) => {
    axios.delete(`http://34.31.128.150:80/messages/${messageId}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
      .then(() => {
        socket.emit('deleteMessage', messageId);
      })
      .catch(error => {
        console.error('Error deleting message:', error);
      });
  };

  const handleLogout = () => {
    localStorage.removeItem('username');
    localStorage.removeItem('token');
    setUsername('');
    setToken('');
  };

  if (!token) {
    return <AuthPage setToken={setToken} setUsername={setUsername} />;
  }

  return (
    <div className="App">
      <header className="header">
        <h1 className="title">Chat App</h1>
        <LogoutButton handleLogout={handleLogout} />
      </header>
      <ChatWindow messages={messages} username={username} onDeleteMessage={handleDeleteMessage} />
      <MessageInput onSendMessage={handleSendMessage} />
    </div>
  );
};

export default App;
