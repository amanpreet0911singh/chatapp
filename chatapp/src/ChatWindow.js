import React, { useRef, useEffect } from 'react';
import './ChatWindow.css';

const ChatWindow = ({ messages, username, onDeleteMessage }) => {
  const chatEndRef = useRef(null);

  const scrollToBottom = () => {
    if (chatEndRef.current) {
      chatEndRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  const handleRightClick = (event, message) => {
    event.preventDefault();
    if (message.username === username) {
      if (window.confirm('Do you want to delete this message?')) {
        onDeleteMessage(message.id);
      }
    }
  };

  return (
    <div className="chat-window">
      {messages.map((message, index) => (
        <div
          key={index}
          className={`message ${message.username === username ? 'my-message' : 'other-message'}`}
          onContextMenu={(event) => handleRightClick(event, message)}
        >
          <span className="username">{message.username}:</span>
          <span className="text">{message.text}</span>
        </div>
      ))}
      <div ref={chatEndRef} /> {/* Ref element for scrolling to bottom */}
    </div>
  );
};

export default ChatWindow;
